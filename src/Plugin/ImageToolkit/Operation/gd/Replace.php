<?php

namespace Drupal\image_replace\Plugin\ImageToolkit\Operation\gd;

use Drupal\Core\Image\ImageInterface;
use Drupal\Core\ImageToolkit\Attribute\ImageToolkitOperation;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\system\Plugin\ImageToolkit\GDToolkit;
use Drupal\system\Plugin\ImageToolkit\Operation\gd\GDImageToolkitOperationBase;

/**
 * Defines GD2 image_replace operation.
 */
#[ImageToolkitOperation(
  id: "image_replace_gd",
  toolkit: "gd",
  operation: "image_replace",
  label: new TranslatableMarkup("Replace"),
  description: new TranslatableMarkup("Swap the original image with a replacement image.")
)]
class Replace extends GDImageToolkitOperationBase {

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, array<string, string>>
   */
  protected function arguments(): array {
    return [
      'replacement_image' => [
        'description' => 'The replacement image',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $arguments
   * @phpstan-return array<string, mixed>
   */
  protected function validateArguments(array $arguments): array {
    if (!($arguments['replacement_image'] instanceof ImageInterface && $arguments['replacement_image']->getToolkit() instanceof GDToolkit)) {
      throw new \InvalidArgumentException("Invalid replacement image specified for the 'image_replace' operation.");
    }
    return $arguments;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $arguments
   */
  protected function execute(array $arguments = []): bool {
    $replacement_image = $arguments['replacement_image'];
    assert($replacement_image instanceof ImageInterface);
    $replacement_toolkit = $replacement_image->getToolkit();
    assert($replacement_toolkit instanceof GDToolkit);
    $replacement_resource = $replacement_toolkit->getImage();
    assert($replacement_resource !== NULL);
    $this->getToolkit()->setImage($replacement_resource);
    return TRUE;
  }

}
