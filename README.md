# Image Replace

Image Replace provides a way to supply optional alternative source images
mapped to image styles. This is useful when building responsive sites with
"art directed" images where cropping and resizing is not enough but images
need to be swapped out completely. For example when delivering graphics
containing rendered text, e.g. responsive hero images.

This module works well with responsive image modules leveraging image styles,
for example the core "Responsive Image" module.

- For a full description of this module, visit the
  [project page](https://www.drupal.org/project/image_replace)
- To submit bug reports and feature suggestions, or track changes, visit the
  [issue tracker](https://www.drupal.org/project/issues/image_replace)
- To learn more about Art direction as a use case for responsive images, refer
  to [MDN](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images#art_direction)

## Table of contents

- Installation
- Configuration
- Maintainers

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

See the contrib modules guide on drupal.org for
[detailed instructions](https://www.drupal.org/docs/contributed-modules/image-replace/).

## Maintainers

Current maintainers:
- [znerol](https://www.drupal.org/u/znerol)
